attribute vec3 position;
attribute vec3 color;

uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;

void main()
{
	// simply pass position and vertex color on to the fragment shader
	gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
}