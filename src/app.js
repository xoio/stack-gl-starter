import {clear,enable} from "./glut"

const raf = require('raf');
const gl = require('webgl-context')({
    width:window.innerWidth,
    height:window.innerHeight
});

document.body.appendChild(gl.canvas);

raf(function tick(){
    clear(gl);
    enable(gl.DEPTH_TEST);
    raf(tick);
});
